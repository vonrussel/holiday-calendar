﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace HolidayCalendarService
{
    public class Email
    {
        private SmtpClient Client {get; set; }

        public Email(SmtpClient client) {
            this.Client = client;
        }

        public static SmtpClient Gmail(string email, string password)
        {
            return new SmtpClient()
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(email, password)
            };
        }


        public void Send(MailMessage message)
        {
            try
            {
                this.Client.Send(message);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
