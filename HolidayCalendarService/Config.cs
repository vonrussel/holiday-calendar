﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HolidayCalendarService
{

    public class HolidayCalendarDbContext : DbContext
    {
        public HolidayCalendarDbContext()
            : base("name=HolidayCalendarDbContext")
        {

        }

        public DbSet<User> Users_ { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<VacationLeave> VacationLeaves { get; set; }
        public DbSet<Settings> Settings_ { get; set; }

    }


}
