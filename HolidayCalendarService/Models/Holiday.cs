﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HolidayCalendarService
{
    public class Holiday
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Date")]
        public DateTime date { get; set; }

        [Display(Name = "Recurring?")]
        public bool repeat_anual { get; set; }

        [Required]
        [Display(Name = "Holiday Name")]
        public string title { get; set; }

        public bool is_deleted { get; set; }


        // methods
        public static List<Holiday> GetAll()
        {
            return Calendar.Database.Holidays.Where(h => h.is_deleted == false).ToList();
        }

        public static List<Holiday> GetAll(DateTime start, DateTime end)
        {
            var db = Calendar.Database;
            var holidays = db.Holidays.Where(
                h => 
                    h.is_deleted == false &&
                    h.repeat_anual == false &&
                    (h.date >= start && h.date <= end)
            ).ToList();

            holidays.AddRange(AppendRecurring(start, end));
            return holidays;
        }

        private static IEnumerable<Holiday> AppendRecurring(DateTime start, DateTime end)
        {
            var db = Calendar.Database;

            // append repeating holidays
            var repeatingHolidays = db.Holidays.Where(
                h =>
                    h.is_deleted == false &&
                    h.repeat_anual == true
            ).ToList();

            // will be used to add recurring holidays
            int startYear = start.Year;
            int endYear = end.Year;

            foreach (var holiday in repeatingHolidays)
            {
                var holidayDateStart = new DateTime(startYear, holiday.date.Month, holiday.date.Day);
                var holidayDateEnd = new DateTime(endYear, holiday.date.Month, holiday.date.Day);

                if (holidayDateStart >= start && holidayDateStart <= end)
                {
                    yield return new Holiday()
                    {
                        date = new DateTime(startYear, holidayDateStart.Month, holidayDateStart.Day),
                        title = holiday.title
                    };

                }
                else if (holidayDateEnd >= start && holidayDateEnd <= end)
                {
                    yield return new Holiday()
                    {
                        date = new DateTime(endYear, holidayDateEnd.Month, holidayDateEnd.Day),
                        title = holiday.title
                    };
                }
            }
        }

        public static void Delete(int id)
        {
            var db = Calendar.Database;
            Holiday holiday = db.Holidays.Find(id);
            holiday.is_deleted = true;
            db.SaveChanges();
        }

        public static bool IsHoliday(DateTime d)
        {
            var db = Calendar.Database;
            int days = db.Holidays.Where(h =>

                (h.date.Year == d.Year &&
                h.date.Month == d.Month &&
                h.date.Day == d.Day) ||

                // repeated anual
                (
                   h.repeat_anual && 
                   h.date.Month == d.Month &&
                   h.date.Day == d.Day
                )

            ).ToArray().Count();
            return days > 0;
        }



        // for testing
        public static void InsertTest()
        {
            var db = Calendar.Database;

            db.Holidays.Add(new Holiday()
            {
                date = new DateTime(2015, 4, 24),
                title = "Test Holiday",
                repeat_anual = false
            });

            db.Holidays.Add(new Holiday()
            {
                date = new DateTime(2015, 4, 26),
                title = "Test Holiday 2",
                repeat_anual = false
            });

            db.SaveChanges();
        }
    }
}
