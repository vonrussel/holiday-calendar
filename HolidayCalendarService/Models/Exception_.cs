﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace HolidayCalendarService
{

    [Serializable]
    public class VacationLeaveException : Exception
    {

        public VacationLeaveException()
            : base() { }

        public VacationLeaveException(string message)
            : base(message) { }

        public VacationLeaveException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public VacationLeaveException(string message, Exception innerException)
            : base(message, innerException) { }

        public VacationLeaveException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected VacationLeaveException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
    

}
