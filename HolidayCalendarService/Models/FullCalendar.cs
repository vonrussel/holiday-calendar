﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidayCalendarService
{
    public class FullCalendar
    {

        public class Event
        {
            public int vlId { get; set; }
            public string title { get; set; }
            public bool allDay { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public bool editable { get; set; }
            public string color { get; set; }

            public string username { get; set; }

            public int userId { get; set; }

            public string status { get; set; }

            public string className { get; set; }
        }
    }
}
