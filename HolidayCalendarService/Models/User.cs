﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HolidayCalendarService
{

    public class User
    {

        public enum RolesEnum
        {
            admin,
            staff
        }

        // Db fields
        [Key]
        public int id { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string username { get; set; }

        [Required]
        [Display(Name = "Password")]
        public string password { get; set; }

        [Column("salt")]
        [StringLength(30, MinimumLength = 3)]
        public string salt { get; set; }

        [Required]
        [Display(Name = "Full name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Role")]
        public string roles { get; set; }
        
        public string calendar_color { get; set; }

        public bool is_deleted { get; set; }

        public int holidayCredits { get; set; }
        public int yearJobRunnerExecuted { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Date Employed")]
        public DateTime dateEmployed { get; set; }

        public bool IsInRole(string role)
        {
            return roles.Split(';').Contains(role);
        }

        public static bool Auth(string username, string password)
        {
            var _user = Calendar.Database.Users_.FirstOrDefault(user => user.username == username && !user.is_deleted);
            if (_user != null)
            {
                return Security_.PasswordHasher.Hash(password, _user.salt) == _user.password;
            }
            return false;
        }

        public static User GetUser(string username)
        {
            var _user = Calendar.Database.Users_.First(user => user.username == username && !user.is_deleted);
            _user.password = null;
            _user.salt = null;
            return _user;
        }

        public static User GetUserById(int id)
        {
            var _user = Calendar.Database.Users_.FirstOrDefault(user => user.id == id && !user.is_deleted);
            if (_user != null)
            {
                _user.password = null;
                _user.salt = null;
            }
            return _user;
        }

        public void Update()
        {
            var db = Calendar.Database;
            var toUpdate = db.Users_.FirstOrDefault(u => u.id == this.id);
            if (toUpdate != null)
            {
                // list only fields allowed for update
                toUpdate.email = this.email;
                toUpdate.name = this.name;
                toUpdate.username = this.username;
                toUpdate.roles = this.roles;
                toUpdate.dateEmployed = this.dateEmployed;
                toUpdate.holidayCredits = this.holidayCredits;
                db.SaveChanges();
            }
        }

        public void Delete()
        {
            var db = Calendar.Database;
            var toRemove = db.Users_.FirstOrDefault(u => u.id == this.id);
            if (toRemove != null)
            {
                toRemove.is_deleted = true;
                db.SaveChanges();
            }
        }

        public static User Current()
        {
            return GetUser(LoggedUsername());
        }

        public static string LoggedUsername()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        public static List<User> GetAll()
        {
            var db = Calendar.Database;
            return db.Users_.Where(c => c.is_deleted == false).ToList();
        }

        public static void Insert(User user)
        {
            var db = Calendar.Database;
            var random = new Random();
            var color = String.Format("#{0:X6}", random.Next(0x1000000));
            string salt = Security_.Salt.Generate();

            // XX update fields here before saving to 
            // db
            user.is_deleted = false;
            user.calendar_color = color;
            
            // excempt newly created user to update holiday credits
            user.yearJobRunnerExecuted = DateTime.Now.Year;
            
            user.password = Security_.PasswordHasher.Hash(user.password, salt);
            user.salt = salt;
            db.Users_.Add(user);
            db.SaveChanges();
        }








        // For testing
        public static void insertTest()
        {
            /*
            var db = new HolidayCalendarService.HolidayCalendarDbContext();
            db.Users.Add(new HolidayCalendarService.User { id = 4, name = "Admin admin", username = "admin"});
            db.SaveChanges();*/
        }

        public static void addAdmin()
        {
            using (HolidayCalendarDbContext db = Calendar.Database)
            {
                db.Users_.Add(new User { name = "Juan", username = "juan", password = "123456789", roles = "staff"});
                db.SaveChanges();
            }
        }
    } 
}
