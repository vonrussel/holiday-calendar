﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidayCalendarService
{
    public class Events
    {
        /*
         * Mostly will be used in fullcalendarjs
        */
        public static List<FullCalendar.Event> GetAll()
        {
            List<FullCalendar.Event> list = new List<FullCalendar.Event>();
            // get holidays from db
            list.AddRange(GetHolidays());
            // get vl of all staff
            list.AddRange(GetStaffVacationLeaves());

            // loop through each event and add unique color per user
            return list;
        }
        
        public static List<FullCalendar.Event> GetAll(DateTime start, DateTime end)
        {
            List<FullCalendar.Event> list = new List<FullCalendar.Event>();
            // get holidays from db
            list.AddRange(GetHolidays(start, end));
            // get vl of all staff
            list.AddRange(GetStaffVacationLeaves(start, end));

            // loop through each event and add unique color per user
            return list;
        }

        public static IEnumerable<FullCalendar.Event> GetHolidays()
        {
            // TODO: add logic for anual holidays
            foreach (var holiday in Holiday.GetAll())
            {
                yield return new FullCalendar.Event()
                {
                    // maybe set colors for holiday as well
                    color = "green",
                    title = holiday.title,
                    start = holiday.date.ToString("yyyy-MM-dd"),
                    // NOTE: Just adding +1 day for end date coz
                    // of the fullcalendarjs library https://code.google.com/p/fullcalendar/issues/detail?id=2079
                    // stated that end date is inclusive
                    end = holiday.date.AddDays(1).ToString("yyyy-MM-dd"),
                    allDay = true
                };
            }
        }

        public static IEnumerable<FullCalendar.Event> GetHolidays(DateTime start, DateTime end)
        {
            foreach (var holiday in Holiday.GetAll(start, end))
            {
                yield return new FullCalendar.Event()
                {
                    // maybe set colors for holiday as well
                    color = "green",
                    title = holiday.title,
                    className = "holiday",
                    start = holiday.date.ToString("yyyy-MM-dd"),
                    end = holiday.date.AddDays(1).ToString("yyyy-MM-dd"),
                    allDay = true
                };
            }
        }

        public static List<FullCalendar.Event> GetStaffVacationLeaves()
        {
            var list = new List<FullCalendar.Event>();
            string color = "";

            foreach(VacationLeave vl in VacationLeave.GetAll()) {
                var status = vl.GetStatus();
                var title = String.Format("({0}) {1} Holiday Request - {2}", vl.GetDaysCount().ToString(), vl.user.name, status.ToString());

                if(status.Equals(HolidayCalendarService.VacationLeave.Statuses.Pending))
                    color = HolidayCalendarService.VacationLeave.DefaultPendingColor;
                else
                {
                    color = vl.user.calendar_color ?? VacationLeave.DefaultEventColor;
                    if (status.Equals(HolidayCalendarService.VacationLeave.Statuses.Approved))
                    {
                        title += " (" + vl.approved_datetime.Value.ToString("MM-dd-yyyy HH:mm tt") + ")";
                    }
                }


                list.Add(
                    new FullCalendar.Event
                    {
                        start = vl.start.ToString("yyyy-MM-dd"),
                        // NOTE: Just adding +1 day for end date coz
                        // of the fullcalendarjs library https://code.google.com/p/fullcalendar/issues/detail?id=2079
                        // stated that end date is inclusive
                        end = vl.end.AddDays(1).ToString("yyyy-MM-dd"),
                        title = title,
                        allDay = true,
                        color = color,
                        username = vl.user.username,
                        userId = vl.user.id,
                        status = vl.GetStatus().ToString()
                    }
                );
            }
            return list;
        }

        public static List<FullCalendar.Event> GetStaffVacationLeaves(DateTime start, DateTime end)
        {
            var list = new List<FullCalendar.Event>();
            string color = "";

            foreach(VacationLeave vl in VacationLeave.GetAll(start, end)) {
                var status = vl.GetStatus();
                string className = "_id_" + vl.id  + " " + status.ToString() + " "; // add id to class
                var title = String.Format("({0}) {1} Holiday Request - {2}", vl.GetDaysCount().ToString(), vl.user.name, status.ToString());

                if (status.Equals(HolidayCalendarService.VacationLeave.Statuses.Pending))
                    color = HolidayCalendarService.VacationLeave.DefaultPendingColor;
                else
                {
                    color = vl.user.calendar_color ?? VacationLeave.DefaultEventColor;
                    if (status.Equals(HolidayCalendarService.VacationLeave.Statuses.Approved))
                    {
                        title += " (" + vl.approved_datetime.Value.ToString("MMM-dd-yyyy HH:mm tt") + ")";
                    }
                    else if (status.Equals(HolidayCalendarService.VacationLeave.Statuses.Disapproved))
                    {
                        color = null;
                    }
                    
                    if (vl.cancelled)
                    {
                        color = null;
                        className += "Cancelled ";
                        title = String.Format("({0}) {1} Holiday Request - Cancelled", vl.GetDaysCount().ToString(), vl.user.name);
                    }
                }

                list.Add(
                    new FullCalendar.Event
                    {
                        vlId = vl.id,
                        start = vl.start.ToString("yyyy-MM-dd"),
                        end = vl.end.AddDays(1).ToString("yyyy-MM-dd"),
                        title = title,
                        allDay = true,
                        color = color,
                        className = className,
                        username = vl.user.username,
                        userId = vl.user.id,
                        status = vl.GetStatus().ToString()
                    }
                );
            }
            return list;
        }

    }
}
