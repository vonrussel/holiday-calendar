﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HolidayCalendarService
{
    public class VacationLeave
    {
        public static string DefaultPendingColor = Settings.Get("event_color_pending");
        public static string DefaultEventColor = Settings.Get("event_color_default");
        public static int DefaultVacationLeaveCount {
            get {
                return int.Parse(Settings.Get("max_vacation_leave"));
            }
            set { }
        }

        public enum Statuses
        {
            Pending,
            Disapproved,
            Approved
        }

        // Db fields
        [Key]
        public int id { get; set; }

        public DateTime start { get; set; } 
        public DateTime end { get; set; }

        public DateTime? approved_datetime { get; set; }
        public bool approved { get; set; }
        public DateTime? applied_datetime { get; set; }

        public bool cancelled { get; set; }

        public int user_id { get; set; }
        [ForeignKey("user_id")]
        public virtual User user { get; set; }

        public int? approved_by_user { get; set; }
        [ForeignKey("approved_by_user")]
        public virtual User approved_by { get; set; }


        // methods
        public static bool Insert(DateTime start, DateTime end, int userId = 0)
        {
            // TODO: validate vl here
            // think of a way to validate just on server then
            // client should validate calling the server validation before
            // submitting the form
            try {
                HolidayCalendarDbContext db = Calendar.Database;
                string loggedUsername = User.LoggedUsername();
                User user;
                
                if(userId == 0) {
                    user = db.Users_.FirstOrDefault(u => u.username == loggedUsername && !u.is_deleted);
                } else {
                    user = db.Users_.FirstOrDefault(u => u.id == userId && !u.is_deleted);
                }

                var vl = new VacationLeave()
                {
                    start = start,
                    end = end,
                    user = user,
                    applied_datetime = DateTime.Now
                };

                db.VacationLeaves.Add(vl);
                db.SaveChanges();

                Calendar.SendRequestEmail(vl);
                return true;

            } catch(Exception ex) {
                return false;
            }
        }
        
        public Statuses GetStatus()
        {
            if (!this.approved)
            {
                if (this.approved_by != null)
                    return Statuses.Disapproved;
                else
                    return Statuses.Pending;
            }
            return Statuses.Approved;
        }

        public static List<VacationLeave> GetPendingRequests() {
            var db = Calendar.Database;
            return db.VacationLeaves.Where(c => c.approved_by == null && !c.cancelled).ToList();
        }

        public static List<VacationLeave> GetAll()
        {
            var db = Calendar.Database;
            return db.VacationLeaves.Where(c => c != null && !c.cancelled).ToList();
        }
        
        public static List<VacationLeave> GetAll(DateTime start, DateTime end)
        {
            var db = Calendar.Database;
            return db.VacationLeaves.Where(
                c => 
                    (c.start >= start && c.end <= end)
            ).ToList();
        }

        public static void ApproveRequest(int id)
        {
            string loggedUsername = User.LoggedUsername();
            var db = Calendar.Database;
            var vl = db.VacationLeaves.FirstOrDefault(v => v.id == id && !v.cancelled);
            var user = db.Users_.FirstOrDefault(u => u.username == loggedUsername);

            // decrement holidays credit
            var requestingUser = db.Users_.FirstOrDefault(u => u.id == vl.user_id);
            requestingUser.holidayCredits -= vl.GetDaysCount();

            if (vl != null)
            {
                vl.approved_by = user;
                vl.approved_datetime = DateTime.Now;
                vl.approved = true;

                // validate if number of days did not exceed
                /*var remaining = DefaultVacationLeaveCount - GetUsedDaysOfUser(vl.user);
                if (vl.GetDaysCount() > remaining)
                {
                    throw new VacationLeaveException("You are approving an exceeding in days request");
                }*/

                db.SaveChanges();
                Calendar.SendRequestApprovalEmail(vl);
            }
        }

        public static void RejectRequest(int id)
        {
            var db = Calendar.Database;
            var vl = db.VacationLeaves.FirstOrDefault(v => v.id == id && !v.cancelled);
            string loggedUsername = User.LoggedUsername();

            if (vl != null)
            {
                // TODO: make db fields naming flexible
                vl.approved_by = db.Users_.FirstOrDefault(user => user.username == loggedUsername);
                vl.approved_datetime = DateTime.Now;
                db.SaveChanges();
                Calendar.SendRequestApprovalEmail(vl);
            }
        }

        public static void CancelRequest(int id)
        {
            var db = Calendar.Database;
            var vl = db.VacationLeaves.FirstOrDefault(v => v.id == id && v.approved);
            if (vl != null)
            {
                vl.cancelled = true;
                // rollback days used
                vl.user.holidayCredits += vl.GetDaysCount();
                db.SaveChanges();
            }
        }

        // Helpers
        public int GetDaysCount()
        {
            // get number of days in vacation leave without counting
            // weekends and holidays
            int count = 0;
            DateTime dayCtr = this.start;
            List<DateTime> approvedDates = new List<DateTime>();
            var db = Calendar.Database;

            do
            {
                if (!Holiday.IsHoliday(dayCtr) && !IsWeekend(dayCtr))
                {
                    var withoutTime = new DateTime(dayCtr.Year, dayCtr.Month, dayCtr.Day);
                    if (!approvedDates.Contains(withoutTime))
                    {
                        approvedDates.Add(withoutTime);
                        count++;
                    }
                }
                dayCtr = dayCtr.AddDays(1);
            } while (dayCtr <= this.end);
            return count;
        }

        public static IEnumerable<VacationLeave> GetApproved(User user)
        {
            var db = Calendar.Database;
            return db.VacationLeaves.Where(vl => vl.user_id == user.id && vl.approved && vl.approved_by != null && !vl.cancelled).ToList();
        }
        
        public static IEnumerable<VacationLeave> GetApprovedThisYear(User user)
        {
            var db = Calendar.Database;
            return db.VacationLeaves.Where(vl =>
                (vl.user_id == user.id && vl.approved && vl.approved_by != null && !vl.cancelled) &&
                vl.start.Year == DateTime.Now.Year
            ).ToList();
        }

        public static IEnumerable<VacationLeave> GetAllApproved(User user)
        {
            var db = Calendar.Database;
            return db.VacationLeaves.Where(vl =>
                (vl.user_id == user.id && vl.approved && vl.approved_by != null && !vl.cancelled)
            ).ToList();
        }

        public static int GetUsedDaysOfUser(User user, bool currentYear = true)
        {
            IEnumerable<VacationLeave> approved;

            if (currentYear)
            {
                // get approved vls for this current year only
                approved = VacationLeave.GetApprovedThisYear(user);
            }
            else
            {
                // get approved vls for this current year only
                approved = VacationLeave.GetAllApproved(user);
            }


            int count = 0;
            List<DateTime> approvedDates = new List<DateTime>();
            foreach (var vl in approved)
            {
                // loop through each days of vl
                var dayCtr = vl.start;
                do
                {

                    if(!Holiday.IsHoliday(dayCtr) && !IsWeekend(dayCtr)) {
                        var withoutTime = new DateTime(dayCtr.Year, dayCtr.Month, dayCtr.Day);

                        if (!approvedDates.Contains(withoutTime))
                        {
                            approvedDates.Add(withoutTime);
                            count++;
                        }
                    }
                    dayCtr = dayCtr.AddDays(1);
                } while (dayCtr <= vl.end);
            }

            return count;
        }

        public static IEnumerable<VacationLeave> GetInBetweenDates(User user, DateTime start, DateTime end)
        {
            // get all vls for this current year only
            var db = Calendar.Database;
            if (user == null)
            {
                user = HolidayCalendarService.User.Current();
            }
            var vacationLeaves = db.VacationLeaves.Where(vl => vl.user_id == user.id).ToList();

            foreach (var vl in vacationLeaves)
            {
                if(
                    !vl.cancelled &&

                    // test if current vacation leave is between passed dates
                    (vl.start >= start && vl.end <= end) || // inside
                    (vl.start < start && vl.end > end) || // occupied

                    (vl.start <= start && vl.end <= end && vl.end >= start) || // left
                    (vl.start >= start && vl.end >= end && end >= vl.start) // right
                ) {
                    yield return vl;
                }
            }
        }

        public static int GetRemainingVacationLeaveDayCount(User _user)
        {
            return GetHolidayCredits(_user);
        }

        public int GetItemRemainingVacationLeaveDayCount()
        {
            return GetHolidayCredits(this.user) - this.GetDaysCount();
        }

        public static int GetHolidayCredits(User user)
        {
            return user.holidayCredits;
        }

        private static bool IsWeekend(DateTime date)
        {
            return date.DayOfWeek.Equals(DayOfWeek.Saturday) || date.DayOfWeek.Equals(DayOfWeek.Sunday);
        }



        // For testing
        public static void insertTest()
        {
            HolidayCalendarDbContext db = Calendar.Database;
            string loggedUsername = User.LoggedUsername();
            db.VacationLeaves.Add(
                new VacationLeave()
                {
                    user = db.Users_.FirstOrDefault(user => user.username == loggedUsername),
                    start = DateTime.Now,
                    end = DateTime.Now
                }
            );
            db.SaveChanges();
        }
    }
}

