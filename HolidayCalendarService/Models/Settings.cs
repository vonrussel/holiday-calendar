﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HolidayCalendarService
{
    public class Settings
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("key")]
        [StringLength(30)]
        public string Key { get; set; }

        [Column("value")]
        public string Value { get; set; }


        public static void Add(Settings settings)
        {
            var ctx = Calendar.Database;
            ctx.Settings_.Add(settings);
            ctx.SaveChanges();
        }

        public static string Get(string key)
        {
            return Calendar.Database.Settings_.FirstOrDefault(s => s.Key == key).Value;
        }
    }
}
