﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Data.Entity;

namespace HolidayCalendarService
{
    // App logic should be here
    public class Calendar
    {
        static string SystemEmail {
            get {
                return Settings.Get("system_email");
            }
        }
            
        static string SystemEmailPassword {
            get {
                return Settings.Get("system_email_password");
            }
        }
            
        public static void Initialize()
        {
            var db = Calendar.Database;
            if (db.Users_.Count() == 0)
            {
                // add 'admin' pw 123456789 as default user
                User.Insert(
                    new User
                    {
                        username = "admin",
                        password = "123456789",
                        name = "Administrator",
                        roles = User.RolesEnum.admin.ToString(),
                        email = "v0nrussel@gmail.com"
                    }

                );
            }

            // if no settings
            if (db.Settings_.Count() == 0)
            {
                Settings.Add(new Settings {
                    Key = "admin_email",
                    Value = "v0nrussel@gmail.com"
                });

                Settings.Add(new Settings
                {
                    Key = "admin_name",
                    Value = "Von Russel"
                });

                // currently just working for gmail
                Settings.Add(new Settings
                {
                    Key = "system_email",
                    Value = "v0nrussel@gmail.com"
                });

                Settings.Add(new Settings
                {
                    Key = "system_email_password",
                    Value = ""
                });

                Settings.Add(new Settings
                {
                    Key = "max_vacation_leave",
                    Value = "20"
                });

                Settings.Add(new Settings
                {
                    Key = "event_color_pending",
                    Value = "gray"
                });

                Settings.Add(new Settings
                {
                    Key = "event_color_default",
                    Value = "blue"
                });
            }
        }

        public static MailAddress AdminEmail {
            get {
                string adminEmail = Settings.Get("admin_email");

                if (String.IsNullOrEmpty(adminEmail))
                {
                    throw new Exception("'admin_email' in db settings does not exist");
                }
                return new MailAddress(adminEmail, Settings.Get("admin_name"));  
            }
            set { }
        }
        
        public static HolidayCalendarDbContext Database {
            get {
                return new HolidayCalendarDbContext();
            }
        }

        // send email to admin to notify leave request
        public static void SendRequestEmail(VacationLeave vl)
        {
            var gmail = Email.Gmail(Calendar.SystemEmail, Calendar.SystemEmailPassword);
            var email = new Email(gmail);

            try { 
                email.Send(
                    new MailMessage(new MailAddress(Calendar.SystemEmail, "Holiday Calendar"), AdminEmail)
                    {
                        Subject = "New request for Vacation Leave",
                        Body = String.Format("{0} has requested vacation leave starting from {1} to {2}",
                            vl.user.name,
                            vl.start.ToString("dd MMM yyyy"),
                            vl.end.ToString("dd MMM yyyy")
                        )
                    }
                );
            } catch(Exception ex) {

            }

        }

        // send email to employee that his request is rejected / approved
        public static void SendRequestApprovalEmail(VacationLeave vl)
        {
            var gmail = Email.Gmail(Calendar.SystemEmail, Calendar.SystemEmailPassword);
            var email = new Email(gmail);

            if (vl.approved)
            {
                try
                {

                    email.Send(
                        new MailMessage(AdminEmail, new MailAddress(vl.user.email, vl.user.name))
                        {
                            Subject = "Request for Vacation Leave",
                            Body = String.Format("Hi {0}, your request for vacation leave is approved by {1}",
                                vl.user.name,
                                vl.approved_by.name
                            )
                        }
                    );
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                try
                {
                    email.Send(
                        new MailMessage(AdminEmail, new MailAddress(vl.user.email, vl.user.name))
                        {
                            Subject = "Request for Vacation Leave",
                            Body = String.Format("Hi {0}, Sorry but your request for vacation leave is disapproved by management",
                                vl.user.name
                            )
                        }
                    );
                }
                catch (Exception ex)
                {

                }
                
            }

        }

        public static void UpdateUserHolidayCredits()
        {
            var db = Calendar.Database;
            int currentYear = DateTime.Now.Year;
            var users = db.Users_.Where(u => u.yearJobRunnerExecuted < currentYear);
            foreach (var user in users)
            {
                // user exists but does not have year index for job task update
                if (user.yearJobRunnerExecuted <= 0)
                {
                    
                }
                else
                {
                    // compute the difference then multiply to number of 
                    // holiday credits to be added annually
                    int years = currentYear - user.yearJobRunnerExecuted;
                    int annualIncrement = int.Parse(Settings.Get("max_vacation_leave"));
                    user.holidayCredits += (years * annualIncrement);
                }

                // excempt this record to update for this year
                user.yearJobRunnerExecuted = currentYear;
            }
            db.SaveChanges();
        }

    }
}
