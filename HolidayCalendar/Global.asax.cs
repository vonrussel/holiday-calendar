﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Hosting;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace HolidayCalendar
{
    public class MvcApplication : System.Web.HttpApplication
    {
        static Func<CancellationToken, Task> workItem = UpdateUserHolidayCredits;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer<HolidayCalendarService.HolidayCalendarDbContext>(null);

            HolidayCalendarService.Calendar.Initialize();

            // execute runner
            HostingEnvironment.QueueBackgroundWorkItem(workItem);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        //let us take out the username now                
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;
                        HolidayCalendarService.User user = HolidayCalendarService.User.GetUser(username: username);
                        roles = user.roles;
 
                        //Let us set the Pricipal with our user specific details
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }
        }



        // task runner for user holiday credits
        private static async Task UpdateUserHolidayCredits(CancellationToken cancellationToken)
        {
            Trace.WriteLine("Update holiday credits of user if not updated");
            HolidayCalendarService.Calendar.UpdateUserHolidayCredits();

            // repeat every 30 mins
            await Task.Delay(1000 * 60 * 30);
            HostingEnvironment.QueueBackgroundWorkItem(workItem);
        } 
    }
}
