﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.ComponentModel;
using Service = HolidayCalendarService;
using HolidayCalendar.Models;

namespace HolidayCalendar.Controllers
{
    public class VacationLeaveController : Controller
    {
        //
        // GET: /VacationLeave/Approve
        [Authorize(Roles="admin")]
        public ActionResult Approve()
        {
            var requests = Service.VacationLeave.GetPendingRequests();
            return View(requests);
        }

        // Homepage
        [Authorize]
        public ActionResult Calendar()
        {
            return View(new RequestForm());
        }

        [Authorize]
        public ActionResult Events(DateTime start, DateTime end)
        {
            // Add 1 month span for events query to minimize query
            start = start.AddMonths(-1);
            end = end.AddMonths(1);
            return Json(Service.Events.GetAll(start, end), JsonRequestBehavior.AllowGet);
        }
        
        [Authorize]
        public ActionResult RequestLeaveSubmitAjax(Models.RequestForm model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // allow admin to change the user selection
                    if (!User.IsInRole("admin"))
                    {
                        model.userId = 0;
                    }
                    Service.VacationLeave.Insert(model.start, model.end, model.userId);
                }
                catch (Service.VacationLeaveException ex)
                {

                }

                return Redirect(Url.Content("~/"));
            }
            return HttpNotFound();
        }

                
        // validate dates first if leave date is already requested
        // this will be called first by ajax call then return json if has
        [Authorize]
        public ActionResult RequestLeavePreValidateAjax(Models.RequestForm model)
        {
            // form did not succeed
            var errors =
                from x in ModelState.Keys
                where ModelState[x].Errors.Count > 0
                select new
                {
                    key = x,
                    errors = ModelState[x].Errors.
                                Select(y => y.ErrorMessage).
                                ToArray()
                };
            return Json(errors);
        }


        [Authorize(Roles="admin")]
        public ActionResult ApproveRequest(int id)
        {
            Service.VacationLeave.ApproveRequest(id);
            return RedirectToAction("Approve", "VacationLeave");
        }

        [Authorize(Roles = "admin")]
        public ActionResult RejectRequest(int id)
        {
            Service.VacationLeave.RejectRequest(id);
            return RedirectToAction("Approve", "VacationLeave");
        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult GetUserHolidayDetails(int id)
        {
            var user = Service.User.GetUserById(id);
            return Json(new
            {
                user = user,
                dateEmployed = user.dateEmployed.ToString("dd MMM yyyy"),
                holidaysUsed = Service.VacationLeave.GetUsedDaysOfUser(user),
                holidayCredits = Service.VacationLeave.GetRemainingVacationLeaveDayCount(user)
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Cancel(int id)
        {
            Service.VacationLeave.CancelRequest(id);
            return RedirectToAction("Calendar");
        }

        // For testing
        [WebMethod]
        public ActionResult tryvalidate(object obj, string model)
        {


            Type type = Type.GetType(model);
            //var ret = (type)obj;
            return Json(
                //ret
                new
                {               
                    
                }
            , JsonRequestBehavior.AllowGet);
        }
	}
}