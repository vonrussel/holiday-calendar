﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using HolidayCalendar.Models;
using Service = HolidayCalendarService;

namespace HolidayCalendar.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/Index
        [Authorize(Roles="admin")]
        public ActionResult Index()
        {
            return View(Service.User.GetAll());
        }

        // Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            return View(new Models.UserView.Create());
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.UserView.Create model)
        {
            if (ModelState.IsValid)
            {
                Service.User.Insert(new Service.User()
                {
                    username = model.username,
                    password = model.password,
                    email = model.email,
                    dateEmployed = model.dateEmployed,
                    name = model.name,
                    roles = model.roles,
                    holidayCredits = model.holidayCredits
                });
                return RedirectToAction("Index", "User");
            }
            return HttpNotFound();
        }

        // Edit
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            var user = Service.User.GetUserById(id);
            return View(new Models.UserView.Edit()
            {
                id = id,
                username = user.username,
                name = user.name,
                dateEmployed = user.dateEmployed,
                email = user.email,
                roles = user.roles,
                holidayCredits = user.holidayCredits
            });
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Models.UserView.Edit model)
        {
            if (ModelState.IsValid)
            {
                var user = Service.User.GetUserById(model.id);
                user.username = model.username;
                user.name = model.name;
                user.roles = model.roles;
                user.dateEmployed = model.dateEmployed;
                user.email = model.email;
                user.holidayCredits = model.holidayCredits;
                user.Update();
                return RedirectToAction("Index", "User");
            }
            return HttpNotFound();
        }

        // Delete
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            Service.User.GetUserById(id).Delete();
            return RedirectToAction("Index", "User");
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginView model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {
                
                string username = model.username;
                string password = model.password;

                // XXX
                // Now if our password was enctypted or hashed we would have done the
                // same operation on the user entered password here, But for now
                // since the password is in plain text lets just authenticate directly

                // User found in the database
                if (HolidayCalendarService.User.Auth(username, password))
                {

                    FormsAuthentication.SetAuthCookie(username, false);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return Redirect(Url.Content("~/"));
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username or password provided is incorrect.");
                }
  
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }

	}
}