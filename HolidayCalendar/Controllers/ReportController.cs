﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HolidayCalendar.Controllers
{
    public class ReportController : Controller
    {

        [Authorize(Roles="admin")]
        public ActionResult YearOverview(int? year)
        {
            ViewBag.Year = year ?? DateTime.Now.Year;
            return View(HolidayCalendarService.User.GetAll());
        }
	}
}