/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="bootstrap.js" />
/// <reference path="respond.js" />
/// <reference path="../content/vendor/moment.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/fullcalendar.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/gcal.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang-all.js" />
/// <reference path="../content/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ar-ma.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ar-sa.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ar-tn.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ar.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/bg.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ca.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/cs.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/da.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/de-at.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/de.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/el.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/en-au.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/en-ca.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/en-gb.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/es.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/fa.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/fi.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/fr-ca.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/fr.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/he.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/hi.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/hr.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/hu.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/id.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/is.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/it.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ja.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ko.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/lt.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/lv.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/nb.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/nl.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/pl.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/pt-br.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/pt.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ro.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/ru.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/sk.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/sl.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/sr-cyrl.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/sr.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/sv.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/th.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/tr.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/uk.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/vi.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/zh-cn.js" />
/// <reference path="../content/vendor/fullcalendar-2.3.1/lang/zh-tw.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="custom/validations.js" />
/// <reference path="custom/jquery.serializeobject.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ar.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.az.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.bg.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.bs.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ca.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.cs.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.cy.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.da.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.de.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.el.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.en-GB.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.et.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.eu.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.fa.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.fi.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.fo.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.fr-CH.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.gl.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.he.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.hr.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.hu.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.hy.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.is.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.it-CH.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.it.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ka.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.kh.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.kk.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.kr.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.lt.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.lv.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.me.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.mk.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ms.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.nb.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.nl-BE.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.nl.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.no.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.pl.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.pt.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ro.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.rs-latin.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.rs.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.sk.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.sl.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.sq.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.sr-latin.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.sr.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.sv.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.sw.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.tr.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.uk.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" />
/// <reference path="../Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js" />
/// <reference path="../Content/vendor/fullcalendar-2.3.1/lib/jquery-ui.custom.min.js" />
/// <reference path="../Content/vendor/fullcalendar-2.3.1/lib/jquery.min.js" />
/// <reference path="../Content/vendor/fullcalendar-2.3.1/lib/moment.min.js" />
/// <reference path="jquery-1.8.0.js" />
/// <reference path="jquery.validate.js" />
