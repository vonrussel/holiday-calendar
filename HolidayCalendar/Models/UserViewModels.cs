﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using Service = HolidayCalendarService;


namespace HolidayCalendar.Models
{
    public class UserView
    {

        public class UserViewParent
        {
            public IEnumerable<SelectListItem> RolesList
            {
                get
                {
                    var roles = Enum.GetValues(typeof(Service.User.RolesEnum)).Cast<Service.User.RolesEnum>().ToArray();
                    foreach (Service.User.RolesEnum role in roles)
                    {
                        string title = role.ToString();
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        yield return new SelectListItem { Text = textInfo.ToTitleCase(title).ToString(), Value = title };
                    }
                }
            }

            [Required, Display(Name = "Date Employed")]
            public DateTime dateEmployed { get; set; }

            [Required, Display(Name = "Holiday credits")]
            public int holidayCredits { get; set; }
        }

        public class Create : UserViewParent
        {
            [Required, StringLength(40), Display(Name = "Username")]
            public string username { get; set; }

            [Required, StringLength(40), Display(Name = "Password")]
            public string password { get; set; }

            [Required, StringLength(40), Display(Name = "Name")]
            public string name { get; set; }

            [Required, StringLength(40), Display(Name = "Email Address")]
            public string email { get; set; }

            [Required, StringLength(40), Display(Name = "Role")]
            public string roles { get; set; }

        }

        public class Edit : UserViewParent
        {
            public int id { get; set; }

            [Required, StringLength(40), Display(Name = "Username")]
            public string username { get; set; }

            [Required, StringLength(40), Display(Name = "Name")]
            public string name { get; set; }

            [Required, StringLength(40), Display(Name = "Email Address")]
            public string email { get; set; }

            [Required, StringLength(40), Display(Name = "Role")]
            public string roles { get; set; }

        }
    }

    public class LoginView
    {
        [Required, StringLength(40), Display(Name = "Username")]
        public string username { get; set; }

        [Required, StringLength(40), Display(Name = "Password")]
        public string password { get; set; }

    }

    


}
