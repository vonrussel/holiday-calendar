﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace HolidayCalendar
{

    public class Validations {

        public class FutureDateAttribute : ValidationAttribute, IClientValidatable
        {
            public override bool IsValid(object value)
            {
                if (HolidayCalendarService.User.Current().IsInRole("admin")) return true;
                return (value != null && Convert.ToDateTime(value).Date >= DateTime.Today);
            }

            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                yield return new ModelClientValidationRule
                {
                    ErrorMessage = ErrorMessage,
                    ValidationType = "futuredate"
                };
            }
        }

        public class DateGreaterThanAttribute : ValidationAttribute, IClientValidatable
        {
            private string compareDateField;
            public DateGreaterThanAttribute(string dateField)
            {
                this.compareDateField = dateField;
            }
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var compareProp = validationContext.ObjectInstance.GetType().GetProperty(this.compareDateField);

                if (Convert.ToDateTime(value).Date >= Convert.ToDateTime(compareProp.GetValue(validationContext.ObjectInstance, null)).Date)
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult(FormatErrorMessage(null));
            }

            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                var clientValidationRule = new ModelClientValidationRule
                {
                    ErrorMessage = ErrorMessage,
                    ValidationType = "dategreaterthan"
                };
                
                // append comparedto date prop
                clientValidationRule.ValidationParameters.Add("comparetodate", compareDateField);

                yield return clientValidationRule;
            }
        }




        // app validation
        public class VacationDateExistsAttribute : ValidationAttribute, IClientValidatable
        {
            protected override ValidationResult IsValid(object value, ValidationContext context)
            {
                var startDateField = context.ObjectInstance.GetType().GetProperty("start");
                var userIdField = context.ObjectInstance.GetType().GetProperty("userId");

                HolidayCalendarService.User user = HolidayCalendarService.User.Current();
                // if current user is admin, allow him to change
                // selected user
                if (user.IsInRole("admin"))
                {
                    int userId = Convert.ToInt32(userIdField.GetValue(context.ObjectInstance, null));
                    user = HolidayCalendarService.User.GetUserById(userId);
                }

                var startDate = Convert.ToDateTime(startDateField.GetValue(context.ObjectInstance, null));
                DateTime endDate;

                if (value != null) {
                    endDate = (DateTime)value;
                    var count = HolidayCalendarService.VacationLeave.GetInBetweenDates(user, startDate, endDate).Count();
                    bool valid =  count == 0;
                    if(valid)
                        return ValidationResult.Success;
                }
                return new ValidationResult(FormatErrorMessage(null));
            }

            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                yield return new ModelClientValidationRule
                {
                    ErrorMessage = ErrorMessage,
                    ValidationType = "vacationdateexists"
                };
            }
        }


     }
}
