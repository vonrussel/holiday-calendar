﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Service = HolidayCalendarService;
using System.Web.Mvc;

namespace HolidayCalendar.Models
{

    public class RequestForm
    {
        // TODO: validate if applied leave is existing
        // 
        [Display(Name = "Leave start date")]
        
        [Validations.FutureDate(ErrorMessage = "Date start must not be in the past")]
        public DateTime start { get; set; }

        [Display(Name = "Leave end date")]
        [Validations.VacationDateExists(ErrorMessage = "Some of the dates you specified are already requested by you")]
        [Validations.DateGreaterThan("start", ErrorMessage = "Date start of leave must be prior to end date")]
        public DateTime end { get; set; }

        [Display(Name = "Selected Staff")]
        public int userId { get; set; }

        public IEnumerable<SelectListItem> UsersSelectList()
        {
            return HolidayCalendarService.User.GetAll().Select(u => {
                return new SelectListItem
                {
                    Selected = u.username == "admin", // set default selected to admin
                    Text = u.name,
                    Value = u.id + ""
                };
            });
        }

        public int GetUsedDaysOfUser()
        {
            return Service.VacationLeave.GetUsedDaysOfUser(Service.User.Current());
        }

        public int GetRemainingDays()
        {
            return Service.VacationLeave.GetRemainingVacationLeaveDayCount(Service.User.Current());
            //return Service.VacationLeave.DefaultVacationLeaveCount - Service.VacationLeave.GetUsedDaysOfUser(Service.User.Current());
        }

        public string GetUserFullName()
        {
            return Service.User.Current().name;
        }

        public Service.User GetUser()
        {
            return Service.User.Current();
        }
    }
}
