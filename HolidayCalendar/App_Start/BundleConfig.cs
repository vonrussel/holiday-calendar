﻿using System.Web;
using System.Web.Optimization;

namespace HolidayCalendar
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/custom/jquery.serializeobject.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Content/vendor/moment.js",
                      "~/Content/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
            
                      "~/Scripts/custom/validations.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css"));

            bundles.Add(new ScriptBundle("~/Content/fullcalendar-scripts").Include(
                      "~/Content/vendor/fullcalendar-2.3.1/fullcalendar.js"));
            bundles.Add(new StyleBundle("~/Content/fullcalendar-styles").Include(
                      "~/Content/vendor/fullcalendar-2.3.1/fullcalendar.css"));
        }
    }
}
