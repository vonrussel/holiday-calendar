﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;

namespace HolidayCalendar
{

    public class ValidateAjaxAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (!filterContext.HttpContext.Request.IsAjaxRequest())
            //    return;

            var modelState = filterContext.Controller.ViewData.ModelState;
            var errorModel =
                from x in modelState.Keys
                where modelState[x].Errors.Count > 0
                select new
                {
                    key = x,
                    errors = modelState[x].Errors.
                                Select(y => y.ErrorMessage).
                                ToArray()
                };

            ///if (errorModel.err==.Count() > 0)
            //{
                filterContext.Result = new JsonResult()
                {
                    Data = errorModel
                };
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                //return;
            //}

            //base.OnActionExecuting(filterContext);
        }
    }
   
}
