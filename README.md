# README #

## Database ##
- Create a MsSQL Database named 'HolidayCalendar'.
- Restore Database backup file located in `@Install` folder.
- Update MSSQL Server user credendtials for using Authentication with username + password in app.config and web.config


# FIX ON DATABASE #

## July 16, 2015 ##
Execute the following to Package Manager Console in Visual Studio with
project 'HolidayCalendarServer' selected
```
add-migration added-date-employed-field
update database
```